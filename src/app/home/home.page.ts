import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  date:  Date = new Date();;
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  monthNames: string[]=[];
  currentMonth: any="";
  currentYear: any;
  currentDate: any;
  currentMonthName:any;

  dataInthisMonths:any="";
  day:any;
  dateName:any;
  eventInthisMonth:any[]=[

    {
      status:"23 OUT OF COMPLIANCE",
      list:[
        {
          text:"5 Contsrctions"
        },
        {
          text:"6 Inspections"
        },
        
      ],
      color:" hsl(0deg 100% 49%)"

    },

    {
      status:"23 IN GRACE PERIOD",
      list:[
        {
          text:"5 IMP"
        },
        {
          text:"6 PUBLIC EVENTS"
        }
      ],
      color:" #ff9800"

    },

  ];

  eventInNextMonth:any[]=[

    {
      status:"30 OUT OF COMPLIANCE",
      list:[
        {
          text:"7 Contsrctions"
        },
        {
          text:"9 Inspections"
        },
        {
          text:"5 Inspections"
        }
      ],
      color:" hsl(0deg 100% 49%)"

    },

    {
      status:"12 IN GRACE PERIOD",
      list:[
        {
          text:"15 IMP"
        },
        {
          text:"6 PUBLIC EVENTS"
        }
      ],
      color:" #ff9800"

    },
    {
      status:"18 IN GRACE PERIOD",
      list:[
        {
          text:"15 IMP"
        },
        {
          text:"6 PUBLIC EVENTS"
        }
      ],
      color:"#e3d55c"

    },

  ];
 
 
  constructor() {}
  ngOnInit() {
   
    this.getDaysOfMonth();
  }


  getDaysOfMonth() {
    // alert(this.date.getMonth());
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.dateName=new Date().toLocaleDateString().split('/')[0];
    this.day= ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"][new Date().getDay()]
  
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentMonthName= new Date().toString().split(' ')[1];  
    this.currentYear = this.date.getFullYear();
    if(this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate();
    } else {
      this.currentDate = 999;
    }

    
  
    var firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
    for(var i = prevNumOfDays-(firstDayThisMonth-1); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push(i);
    }
  
    var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDate();
    for (var i = 0; i < thisNumOfDays; i++) {
      this.daysInThisMonth.push({data:this.eventInthisMonth,date :i+1});

      this.dataInthisMonths=this.daysInThisMonth[0];
    }
  
    var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDay();
    var nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0).getDate();
    for (var i = 0; i < (6-lastDayThisMonth); i++) {
      this.daysInNextMonth.push({data:this.eventInNextMonth,date:i+1} );
    }
    var totalDays = this.daysInLastMonth.length+this.daysInThisMonth.length+this.daysInNextMonth.length;
    if(totalDays<36) {
      for(var i = (7-lastDayThisMonth); i < ((7-lastDayThisMonth)+2); i++) {
        this.daysInNextMonth.push({data:this.eventInNextMonth,date:i});
      }
    }
  }

  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    this.getDaysOfMonth();
  }
  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0);
    this.getDaysOfMonth();
  }

  onNextMonth(nextDay){
    this.dataInthisMonths=nextDay;

  }
}
